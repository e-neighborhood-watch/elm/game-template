module Model.Suspended.Type
  exposing
    ( SuspendedModel (..)
    )


import Level.Model.Type
  as Level


type SuspendedModel
  = LevelSelect
  | Level Level.Model
