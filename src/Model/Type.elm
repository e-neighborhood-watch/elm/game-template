module Model.Type
  exposing
    ( Model
    , InnerModel (..)
    )


import Level.Model.Type
  as Level
import LevelSelect.Model.Type
  exposing
    ( WithLevels
    )
import Model.Suspended.Type
  exposing
    ( SuspendedModel
    )
import Settings.Model.Type
  as Settings
import Settings.Type
  exposing
    ( WithSettings
    )


type InnerModel
  = LevelSelect
  | Level Level.Model
  | Settings SuspendedModel Settings.Model


type alias Model =
  WithSettings
    ( WithLevels
      { model :
        InnerModel
      }
    )
