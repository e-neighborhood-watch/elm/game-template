module LevelSelect.View
  exposing
    ( view
    )


import Document.Type
  exposing
    ( Document
    )


import Data.Text.Type
  as Text
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import Settings.Language
  as Language
import Settings.Language.Type
  exposing
    ( Language
    )


view : Language -> Model -> Document Message
view lang _ =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText lang Text.LevelSelectTitle
    , viewBox =
      viewBox
    , body =
      [
      ]
    }
