module LevelSelect.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict


import Keys
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )


subscriptions : Settings -> Model -> Sub Message
subscriptions settings {} =
  Sub.batch
    [ Dict.singleton settings.controls.levelSelectPrevious Previous
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectNext Next
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectSelect Select
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]
