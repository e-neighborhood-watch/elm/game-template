module Data.Levels.Level1
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Text.Type
  as Text
import Level.Type
  exposing
    ( Level
    )


level : Level
level =
  { tileMap =
    Dict.empty
  , itemMap =
    Dict.empty
  , bound =
    { x =
      7
    , y =
      5
    }
  , playerLocation =
    { x =
      0
    , y =
      0
    }
  , title =
    Text.Level1Title
  , description =
    Nothing
  }
