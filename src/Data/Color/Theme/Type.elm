module Data.Color.Theme.Type
  exposing
    ( ThemeColor (..)
    )


type ThemeColor
  = Background
  | Foreground
