module Data.Defs
  exposing
    ( defs
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


defs : Svg a
defs =
  Svg.defs
    []
    [ Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/SettingsWheel.svg#content"
      , SvgAttr.id "SettingsWheel"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/CancelIcon.svg#content"
      , SvgAttr.id "CancelIcon"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/AcceptIcon.svg#content"
      , SvgAttr.id "AcceptIcon"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Character.svg#content"
      , SvgAttr.id "Character"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    ]
