module Animation.Animated.Update
  exposing
    ( update
    )


import Animation.Animated.Type
  exposing
    ( Animated
    )


update : Int -> Animated a -> Maybe (Animated a)
update newTime animated =
  if
    newTime >= animated.endingTime
  then
    Nothing
  else
    Just
      { animated
      | currentTime =
        newTime
      }
