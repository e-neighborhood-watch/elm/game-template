module Animation.Animated.Type
  exposing
    ( Animated
    )


type alias Animated a =
  { a
  | currentTime :
    Int
  , duration :
    Int
  , endingTime :
    Int
  }
