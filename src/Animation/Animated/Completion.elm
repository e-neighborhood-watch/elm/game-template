module Animation.Animated.Completion
  exposing
    ( completion
    )


import Animation.Animated.Type
  exposing
    ( Animated
    )


completion : Animated a -> Float
completion { currentTime, duration, endingTime } =
  toFloat (endingTime - currentTime) / toFloat duration
    |> max 0
