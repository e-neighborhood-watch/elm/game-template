module Level.Model.Init
  exposing
    ( init
    )


import Level.Model.Type
  exposing
    ( Model
    )
import Level.Type
  exposing
    ( Level
    )


init : Level -> Model
init level =
  { level =
    level
  , playerAnimation =
    Nothing
  }
