module Level.Model.Type
  exposing
    ( Model
    )


import Animation.Movement.Type
  as Animation
import Level.Type
  exposing
    ( Level
    )


type alias Model =
  { level : Level
  , playerAnimation :
    Maybe Animation.Movement
  }
