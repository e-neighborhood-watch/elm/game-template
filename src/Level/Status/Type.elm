module Level.Status.Type
  exposing
    ( Status (..)
    , WithStatus
    , add
    )


import Level.Type
  exposing
    ( Level
    )

type Status
  = Complete
  | Incomplete


type alias WithStatus l =
  { l
  | status :
    Status
  }


add : Status -> Level -> WithStatus Level
add status lvl =
  { status =
    status
  , tileMap =
    lvl.tileMap
  , itemMap =
    lvl.itemMap
  , bound =
    lvl.bound
  , playerLocation =
    lvl.playerLocation
  , title =
    lvl.title
  , description =
    lvl.description
  }
