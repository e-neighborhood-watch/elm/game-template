module Level.Type
  exposing
    ( Level
    , get
    )


import Data.Text.Type
  exposing
    ( Text
    )
import Level.Item.Type
  exposing
    ( Item
    )
import Level.Tile.Type
  exposing
    ( Tile
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias Level =
  { tileMap :
    Map Int Tile
  , itemMap :
    Map Int Item
  , bound :
    Position Int
  , playerLocation :
    Position Int
  , title :
    Text
  , description :
    Maybe Text
  }


get :
  { a
  | tileMap :
    Map Int Tile
  , itemMap :
    Map Int Item
  , bound :
    Position Int
  , playerLocation :
    Position Int
  , title :
    Text
  , description :
    Maybe Text
  }
    -> Level
get x =
  { tileMap =
    x.tileMap
  , itemMap =
    x.itemMap
  , bound =
    x.bound
  , playerLocation =
    x.playerLocation
  , title =
    x.title
  , description =
    x.description
  }
