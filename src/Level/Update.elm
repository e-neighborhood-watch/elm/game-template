module Level.Update
  exposing
    ( update
    )


import Task
import Time


import Animation.Animated.Update
  as Animated
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Update.Result.Type
  as Update
import Util.Box.Within
  as Box
import Util.Offset.FromDirection
  as Offset
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Move
  as Position


update : Message -> Model -> Update.Result
update msg { level, playerAnimation } =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange

    Message.Exit ->
      Update.Exit

    Message.TimePasses newTime ->
      case
        playerAnimation
      of
        Nothing ->
          Update.NoChange

        Just animation ->
          Update.InternalChange
            { level =
              level
            , playerAnimation =
              Animated.update newTime animation
            }
            Cmd.none

    Message.Move dir ->
      let
        newLocation =
          Position.move dir level.playerLocation
        levelBox =
          { min =
            { x =
              0
            , y =
              0
            }
          , max =
            level.bound
          }
      in
        if
          Box.within levelBox newLocation
        then
          Time.now
            |> Task.perform (Time.posixToMillis >> Message.AddPlayerAnimation dir)
            |>
              Update.InternalChange
                { level =
                  { level
                  | playerLocation =
                    Position.move dir level.playerLocation
                  }
                , playerAnimation =
                  playerAnimation
                }
        else
          Update.NoChange

    Message.AddPlayerAnimation dir currentTime ->
      let
        offset : Offset Float
        offset =
          dir
            |> Offset.fromDirection
            |> Offset.scale -1
            |> Offset.toFloat
      in
        Update.InternalChange
          { level =
            level
          , playerAnimation =
            Just
              { currentTime =
                currentTime
              , duration =
                50
              , endingTime =
                50 + currentTime
              , dx =
                offset.dx
              , dy =
                offset.dy
              }
          }
          Cmd.none
