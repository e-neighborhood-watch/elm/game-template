module Level.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict
import Time


import Keys
import Level.Message.Type
  exposing
    ( Message (..)
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction


subscriptions : Settings -> Model -> Sub Message
subscriptions settings { playerAnimation } =
  Sub.batch
    [ case
        playerAnimation
      of
        Nothing ->
          Sub.none

        Just animation ->
          Time.posixToMillis >> TimePasses
            |> Browser.onAnimationFrame
    , Direction.North
      |> Move
      |> Dict.singleton settings.controls.playerMoveNorth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.South
      |> Move
      |> Dict.singleton settings.controls.playerMoveSouth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.East
      |> Move
      |> Dict.singleton settings.controls.playerMoveEast
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.West
      |> Move
      |> Dict.singleton settings.controls.playerMoveWest
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]
