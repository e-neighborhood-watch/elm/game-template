module Message.Type
  exposing
    ( Message (..)
    )


import Level.Message.Type
  as Level
import LevelSelect.Message.Type
  as LevelSelect
import Settings.Message.Type
  as Settings


type Message
  = Noop
  | OpenSettings
  | Level Level.Message
  | LevelSelect LevelSelect.Message
  | Settings Settings.Message
