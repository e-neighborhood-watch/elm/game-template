module Settings.Font.Size.Css
  exposing
    ( toPx
    )


import Css
  exposing
    ( Px
    )


import Data.Font.Size
  as Font
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Font.Type
  exposing
    ( Font
    )


toPx : Font -> FontSize -> Px
toPx font fontSize =
  Css.px (Font.rawSize font fontSize)
