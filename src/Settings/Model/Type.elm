module Settings.Model.Type
  exposing
    ( Model
    )


import Settings.Type
  exposing
    ( Settings
    )

type alias Model =
  { settings : Settings
  }
