module Settings.Language
  exposing
    ( fromText
    )


import Settings.Language.EN
  as EN
import Settings.Language.Type
  exposing
    ( Language (..)
    )
import Data.Text.Type
  exposing
    ( Text
    )


fromText : Language -> Text -> String
fromText lang =
  case
    lang
  of
    EN ->
      EN.fromText
