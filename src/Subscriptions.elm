module Subscriptions
  exposing
    ( subscriptions
    )


import Level.Subscriptions
  as Level
import LevelSelect.Subscriptions
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Message.Type
  exposing
    ( Message (..)
    )


subscriptions : Model -> Sub Message
subscriptions { model, levels, settings } =
  case
    model
  of
    Model.Level level ->
      level
        |> Level.subscriptions settings
        |> Sub.map Level

    Model.LevelSelect ->
      levels
        |> LevelSelect.subscriptions settings
        |> Sub.map LevelSelect

    Model.Settings _ _ ->
      Sub.none
